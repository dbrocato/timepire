Timepire

Play as the Timepire, a time vampire who survives by draining time from the flail-swinging men trying to put him down for good. Be careful though! Don't become too young, or you'll never have been born!

To try it out, download the Build folder and run the game executable.

Controls:

A - Move Left

D - Move Right

Space - Jump

Left Click (Hold) - Time Drain

F - Slash

R when in Golden Age - Chrono Cannon