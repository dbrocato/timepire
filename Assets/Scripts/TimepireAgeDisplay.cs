﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimepireAgeDisplay : MonoBehaviour
{
    private Age age;
    private Text text;
    private Vector2 vector = new Vector2(0, 0);
    private RectTransform tf;

    void Awake() {
        GameObject timepire = GameObject.Find("Timepire");
        age = timepire.GetComponentInChildren<Age>();
        tf = GetComponent<RectTransform>();
        Debug.Log(tf.position.x);

        vector.Set(AgeToPosition(age.CurrentAge), tf.position.y);
        tf.position = vector;
    }

    void Update() {
        //text.text = age.CurrentAge.ToString();
        vector.Set(AgeToPosition(age.CurrentAge), tf.position.y);
        tf.position = vector;
    }

    float AgeToPosition(float age) {
        return 3.975f * age + 69.7f;
    }
}
