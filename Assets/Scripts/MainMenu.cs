﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
using UnityEditor;

public class MainMenu : MonoBehaviour
{
    public void Play()
    {

        SceneManager.LoadScene("The_City");

    }

    public void Exit()
    {
       
        Application.Quit();

    }

}
