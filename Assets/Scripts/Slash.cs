﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slash : Attack
{
    [SerializeField] private KeyCode slashKey;
    [SerializeField] private float anticipation;
    [SerializeField] private float duration;
    [SerializeField] private float ageInflicted;

    private Attack[] attacks;

    private Animator slashAnim;
    private Animator timepireAnim;
    private AudioSource sound;
    private Age age;
    private Movement movement;
    private Collider2D slashCollider;

    void Awake() {

        attacks = transform.root.GetComponentsInChildren<Attack>();

        slashAnim = GetComponent<Animator>();
        timepireAnim = transform.root.GetComponent<Animator>();
        sound = GetComponent<AudioSource>();
        age = transform.root.GetComponentInChildren<Age>();
        movement = transform.root.GetComponent<Movement>();
        slashCollider = GetComponent<Collider2D>();

        slashCollider.enabled = false;
    }

    void Update() {
        
        if (Input.GetKeyDown(slashKey) && movement.IsGrounded() && !IsAlreadyAttacking()) {
            StartCoroutine(SlashAttack());
        }
    }

    void OnDestroy() {
        StopAllCoroutines();
    }

    private bool IsAlreadyAttacking() {
        for (int i = 0; i < attacks.Length; i++) {
            if (attacks[i].Attacking) {
                return true;
            }
        }
        return false;
    }

    IEnumerator SlashAttack() {

        Attacking = true;
        movement.LockFlip = true;
        timepireAnim.SetTrigger("Slash");
        yield return new WaitForSeconds(anticipation);
        sound.Play();
        slashAnim.SetTrigger("Slash");
        slashCollider.enabled = true;
        yield return new WaitForSeconds(duration);
        slashCollider.enabled = false;
        movement.LockFlip = false;
        Attacking = false;
    }

    void OnTriggerEnter2D(Collider2D coll) {
        coll.GetComponent<Age>().AddToAge(ageInflicted);
    }
}
