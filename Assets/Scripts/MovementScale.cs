﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScale
{
    private List<MovementScalar> scalars = new List<MovementScalar>();

    public float Value {
        get {
            float val = 1;
            for (int i = 0; i < scalars.Count; i++) {
                if (scalars[i].Active) {
                    val *= scalars[i].Value;
                }
            }
            return val;
        }
    }

    public MovementScale() {

    }

    public void AddScalar(MovementScalar scalar) {
        scalars.Add(scalar);
    }
}
