﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlailSwing : Attack
{
    [SerializeField] private float attackRange;
    [SerializeField] private float anticipation;
    [SerializeField] private float duration;
    [SerializeField] private float recovery;
    [SerializeField] private float ageInflicted;
    [SerializeField] private Cooldown cooldown;

    private Attack[] attacks;

    private Transform target;

    private Animator anim;
    private AudioSource sound;
    private Age age;
    private Movement movement;
    private Collider2D flailCollider;

    void Awake() {

        target = GameObject.Find("Timepire").transform;

        attacks = transform.root.GetComponentsInChildren<Attack>();

        anim = transform.root.GetComponent<Animator>();
        sound = GetComponent<AudioSource>();
        age = transform.root.GetComponentInChildren<Age>();
        movement = transform.root.GetComponent<Movement>();
        flailCollider = GetComponent<Collider2D>();

        flailCollider.enabled = false;
    }

    void Update() {
        
        if (cooldown.IsOffCooldown() && !age.Dying && InAttackingRange() && movement.IsGrounded() && !IsAlreadyAttacking()) {
            StartCoroutine(SlashAttack());
        }
    }

    void OnDestroy() {
        StopAllCoroutines();
    }

    private bool IsAlreadyAttacking() {
        for (int i = 0; i < attacks.Length; i++) {
            if (attacks[i].Attacking) {
                return true;
            }
        }
        return false;
    }

    private bool InAttackingRange() {
        return target != null && Vector2.Distance(transform.position, target.position) <= attackRange;
    }

    IEnumerator SlashAttack() {

        Attacking = true;
        movement.Immobilized = true;
        movement.LockFlip = true;
        anim.SetTrigger("Attack");
        yield return new WaitForSeconds(anticipation);
        flailCollider.enabled = true;
        yield return new WaitForSeconds(duration);
        flailCollider.enabled = false;
        yield return new WaitForSeconds(recovery);
        movement.LockFlip = false;
        movement.Immobilized = false;
        Attacking = false;

        cooldown.StartCooldown();
    }

    void OnTriggerEnter2D(Collider2D coll) {

        Age hitAge = coll.GetComponent<Age>();

        if (hitAge != null) {
            hitAge.AddToAge(ageInflicted);
            sound.Play();
        }
    }
}
