﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEnemyMovement : Movement
{
    [Space(10)]
    [SerializeField] private float stoppingDistance;

    private Transform target;
    private Rigidbody2D rb;

    private Vector2 movementVector = new Vector2(0, 0);

    void Awake() {

        target = GameObject.Find("Timepire").transform;

        rb = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate() {

        if (!Immobilized && IsGrounded() && target != null && Vector2.Distance(transform.position, target.position) > stoppingDistance) {

            float input = Mathf.Sign(target.position.x - transform.position.x);

            if (input != direction) {
                Flip();
            }

            movementVector.Set(input * movementSpeed * Scale.Value, rb.velocity.y);

            rb.velocity = movementVector;
        }
        else {
            rb.velocity = new Vector2(0, rb.velocity.y);
        }
    }
}
