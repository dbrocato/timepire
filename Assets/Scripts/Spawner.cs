﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Spawner : MonoBehaviour
{
    
    [SerializeField] private float minDelay = 5f; 
    [SerializeField] private float delayModifier;
    [SerializeField] private GameObject Enemy1; 

    
    private GameObject[] Enemies = new GameObject[2];
    private GameObject[] spawnPoints;
    private float Clock;
    private float spawnDelay;

    // Start is called before the first frame update
    void Awake()
    {
        Enemies[0] = Enemy1;

        spawnDelay = minDelay;
        Clock = 0f; 
        spawnPoints = GameObject.FindGameObjectsWithTag("spawnPoint");

    }

    
    void Update()
    {

        Clock += Time.deltaTime;
        if(Clock > spawnDelay)
        {

            Spawn();
            Clock = 0f;

        }
        spawnDelay = minDelay + (delayModifier * GameObject.FindGameObjectsWithTag("Enemy").Length);
    }

    void Spawn()
    {

        int spawnpoint = Random.Range(0,spawnPoints.Length);
        int randomEnemy = Random.Range(0,Enemies.Length);
        Instantiate(Enemies[randomEnemy], spawnPoints[spawnpoint].transform.position, Quaternion.identity);

    }

}
