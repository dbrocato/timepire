﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgeDisplay : MonoBehaviour
{
    private Age age;
    private TextMesh text;

    Vector3 normal = new Vector3(1, 1, 1);
    Vector3 reflected = new Vector3(-1, 1, 1);

    void Awake() {
        age = GetComponent<Age>();
        text = GetComponent<TextMesh>();
    }

    void Update() {

        if (transform.root.localScale.x == -1) {
            transform.localScale = reflected;
        }
        else {
            transform.localScale = normal;
        }

        text.text = age.CurrentAge.ToString();
    }
}
