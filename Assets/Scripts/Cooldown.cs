﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Cooldown
{
    [SerializeField] private float cooldownDuration;
    private float cooldownStartStamp = 0;

    public bool IsOffCooldown() {
		return cooldownStartStamp + cooldownDuration <= Time.time;
	}

	public void StartCooldown() {
		cooldownStartStamp = Time.time;
	}

    public float RemainingCooldown() {
        if (IsOffCooldown()) {
            return 0;
        }
        else {
            return cooldownStartStamp + cooldownDuration - Time.time;
        }
    }

    public void SetNewDuration(float newCooldownDuration) {
        cooldownDuration = newCooldownDuration;
    }
}
