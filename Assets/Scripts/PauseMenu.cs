﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{

    [SerializeField] private KeyCode PauseButton;
    GameObject[] PauseUI;

    private AudioSource Music;

    void Awake()
    {

        Time.timeScale = 1;
        PauseUI = GameObject.FindGameObjectsWithTag("Paused");
        hidePause();
        Music = GetComponent<AudioSource>();

    }


    void Update()
    {

        if(Input.GetKeyDown(PauseButton))
        {

            if(Time.timeScale == 0)
            {
                Time.timeScale = 1;
                hidePause();
                Music.Play();

            }
            
            else if(Time.timeScale == 1)
            {
                Time.timeScale = 0;
                showPause();
                Music.Pause();
            }

        }

    }

    void showPause() //Shows the Pause Menu User Interface
    {
        foreach(GameObject g in PauseUI)
            g.SetActive(true);
    }
    
    void hidePause() //Hides the Pause Menu User Interface
    {
        foreach(GameObject g in PauseUI)
            g.SetActive(false);
    }


    public void Reload() //Restarts the Current Level
    {

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }

    public void mainMenu() //Returns the player to the Main Menu
    {

        SceneManager.LoadScene("MainMenu");
        SceneManager.SetActiveScene(SceneManager.GetActiveScene());

    }

    public void Resume() //Unpauses the game
    {

        Time.timeScale = 1;
        hidePause();
        Music.Play();

    }

}
