﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeDrain : Attack
{
    [SerializeField] private KeyCode drainKey;
    [Space(10)]
    [SerializeField] private float ageIncreasePerTick;
    [SerializeField] private float ageDrainRatio;
    [Space(10)]
    [SerializeField] private MovementScalar movementScalar;

    private Attack[] attacks;

    private TimepireMovement movement;
    private Age age;
    private Collider2D drainCollider;
    private Animator drainAnim;
    private Animator timepireAnim;
    private AudioSource sound;

    private float unaffectedMovementScale;

    void Awake() {

        attacks = transform.root.GetComponentsInChildren<Attack>();

        movement = transform.root.GetComponent<TimepireMovement>();
        age = transform.root.GetComponentInChildren<Age>();
        drainCollider = GetComponent<Collider2D>();
        drainAnim = GetComponent<Animator>();
        sound = GetComponent<AudioSource>();
        timepireAnim = transform.root.GetComponent<Animator>();

        drainCollider.enabled = false;
    }

    void Start() {

        movement.Scale.AddScalar(movementScalar);
        movementScalar.Active = false;
    }

    void Update() {

        if (movement.IsGrounded()) {

            if (Input.GetKeyDown(drainKey) && !IsAlreadyAttacking()) {
                Attacking = true;
                drainAnim.SetBool("Drain", true);
                timepireAnim.SetBool("Drain", true);
                movementScalar.Active = true;
                movement.LockFlip = true;
                movement.ForceGrounded = true;
                drainCollider.enabled = true;
                sound.Play();
            }
            else if (Input.GetKeyUp(drainKey)) {
                Attacking = false;
                drainAnim.SetBool("Drain", false);
                timepireAnim.SetBool("Drain", false);
                movementScalar.Active = false;
                movement.LockFlip = false;
                movement.ForceGrounded = false;
                drainCollider.enabled = false;
                sound.Stop();
            }
        }
    }

    private bool IsAlreadyAttacking() {
        for (int i = 0; i < attacks.Length; i++) {
            if (attacks[i].Attacking) {
                return true;
            }
        }
        return false;
    }

    void OnTriggerStay2D(Collider2D coll) {
        coll.GetComponent<Age>().AddToAge(ageIncreasePerTick);
        age.AddToAge(-ageIncreasePerTick * ageDrainRatio);
    }
}
