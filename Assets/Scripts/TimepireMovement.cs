﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimepireMovement : Movement
{
    [Space(10)]
    [SerializeField] private float initialJumpSpeed;
    [Space(10)]
    [SerializeField] private KeyCode jumpKey;

    private Rigidbody2D rb;
    private Animator anim;
    
    private Vector2 movementVector = new Vector2(0, 0);
    private bool jump;
    private bool airborne = false;

    void Awake() {

        rb = GetComponent<Rigidbody2D>();
        anim = transform.root.GetComponent<Animator>();

        LockFlip = false;
        ForceGrounded = false;
    }

    void Update() {

        if (!Immobilized && Input.GetKeyDown(jumpKey) && IsGrounded()) {
            jump = true;
        }
    }

    void FixedUpdate() {

        float input = Input.GetAxisRaw("Horizontal");
        if (!LockFlip && input != 0 && direction != input) {
            Flip();
        }

        if (airborne) {
            airborne = !IsGrounded();
            if (!airborne) {
                anim.SetBool("Jump", false);
            }
        }

        if (!Immobilized && input != 0) {
            anim.SetBool("Walk", true);
            movementVector.Set(movementSpeed * input * Scale.Value, rb.velocity.y);
            rb.velocity = movementVector;
        }
        else {
            anim.SetBool("Walk", false);
            movementVector.Set(0, rb.velocity.y);
            rb.velocity = movementVector;
        }

        if (!ForceGrounded && jump) {
            airborne = true;
            anim.SetTrigger("JumpStart");
            anim.SetBool("Jump", true);
            movementVector.Set(rb.velocity.x, initialJumpSpeed);
            rb.velocity = movementVector;
        }
        jump = false;
    }
}
