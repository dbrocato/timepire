﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Attack : MonoBehaviour
{
    private bool attacking = false;
    public bool Attacking {
        get {
            return attacking;
        }
        set {
            attacking = value;
        }
    }
}
