﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class Play_Again : MonoBehaviour
{

    void Update()
    {

        if(Input.anyKey)
            Continue();

    }

    public void Continue()
    {

        SceneManager.LoadScene("MainMenu");
        SceneManager.SetActiveScene(SceneManager.GetActiveScene());

    }

}
