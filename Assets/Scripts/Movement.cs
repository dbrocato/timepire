﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Movement : MonoBehaviour
{
    [SerializeField] protected float movementSpeed;

    [Space(10)]
    [SerializeField] private float raycastDistance;
    [SerializeField] private LayerMask groundLayer;

    public bool LockFlip { get; set; }
    public bool ForceGrounded { get; set; }
    public bool Immobilized { get; set; }

    private MovementScale scale = new MovementScale();
    public MovementScale Scale {
        get {
            return scale;
        }
    }

    protected float direction = 1;

    public void Flip() {
        direction = -direction;
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }

    public bool IsGrounded() {

        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, raycastDistance, groundLayer);
        return hit;
    }
}
