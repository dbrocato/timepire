﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MovementScalar
{
    [SerializeField] private float value;
    public float Value {
        get {
            return value;
        }
        set {
            this.value = value;
        }
    }


    private bool active = true;
    public bool Active {
        get {
            return active;
        }
        set {
            active = value;
        }
    }
}
