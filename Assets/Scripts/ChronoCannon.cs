﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChronoCannon : Attack
{
    [SerializeField] private KeyCode chronoCannonKey;
    [SerializeField] private float anticipation;
    [SerializeField] private float duration;
    [SerializeField] private float flightDuration;
    [SerializeField] private float explosionDuration;
    [SerializeField] private float recovery;
    [SerializeField] private float ageCost;
    [SerializeField] private float ageInflicted;

    private Attack[] attacks;

    private Animator chronoCannonAnim;
    private Animator timepireAnim;
    private AudioSource sound;
    private Age age;
    private Movement movement;
    private Collider2D chronoCannonCollider;

    void Awake() {

        attacks = transform.root.GetComponentsInChildren<Attack>();

        chronoCannonAnim = GetComponent<Animator>();
        timepireAnim = transform.root.GetComponent<Animator>();
        sound = GetComponent<AudioSource>();
        age = transform.root.GetComponentInChildren<Age>();
        movement = transform.root.GetComponent<Movement>();
        chronoCannonCollider = GetComponent<Collider2D>();

        chronoCannonCollider.enabled = false;
    }

    void Update() {
        
        if (age.GoldenAge && Input.GetKeyDown(chronoCannonKey) && movement.IsGrounded() && !IsAlreadyAttacking()) {
            age.AddToAge(ageCost);
            StartCoroutine(ChronoCannonAttack());
        }
    }

    void OnDestroy() {
        StopAllCoroutines();
    }

    private bool IsAlreadyAttacking() {
        for (int i = 0; i < attacks.Length; i++) {
            if (attacks[i].Attacking) {
                return true;
            }
        }
        return false;
    }

    IEnumerator ChronoCannonAttack() {

        sound.Play();
        Attacking = true;
        movement.LockFlip = true;
        movement.Immobilized = true;
        timepireAnim.SetTrigger("Chrono Cannon");
        yield return new WaitForSeconds(anticipation);
        yield return new WaitForSeconds(duration);
        StartCoroutine(ChronoCannonProjectile());
        yield return new WaitForSeconds(recovery);
        movement.Immobilized = false;
        movement.LockFlip = false;
        Attacking = false;
    }

    IEnumerator ChronoCannonProjectile() {
        chronoCannonAnim.SetTrigger("Chrono Cannon");
        yield return new WaitForSeconds(flightDuration);
        chronoCannonCollider.enabled = true;
        yield return new WaitForSeconds(explosionDuration);
        chronoCannonCollider.enabled = false;
    }

    void OnTriggerEnter2D(Collider2D coll) {
        coll.GetComponent<Age>().AddToAge(ageInflicted);
    }
}
