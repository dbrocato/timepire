﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class Age : MonoBehaviour
{
    [SerializeField] private float initialAge;
    [SerializeField] private float passiveAgeIncrement;
    [SerializeField] private float passiveAgeInterval;
    [SerializeField] private float deathCeilingThreshold;
    [SerializeField] private float deathDelay;
    [Space(10)]
    [SerializeField] private float movementImpact;
    [SerializeField] private MovementScalar movementScalar;

    private Movement movement;
    private Animator anim;
    private AudioSource sound;
    private Collider2D ageHitbox;

    public float CurrentAge { get; private set; }
    public bool Dying { get; private set; }

    public bool GoldenAge {
        get {
            return CurrentAge <= 30 && CurrentAge >= 20;
        }
    }

    void Awake() {

        movement = transform.root.GetComponent<Movement>();
        anim = transform.root.GetComponent<Animator>();
        sound = GetComponent<AudioSource>();
        ageHitbox = GetComponent<Collider2D>();

        CurrentAge = initialAge;
        Dying = false;
        StartCoroutine(PassiveAge());
    }

    void Start() {
        movementScalar.Value = 500 * Mathf.Pow(1.1f * CurrentAge + 64, -1.25f) - 0.6f;
        movement.Scale.AddScalar(movementScalar);
    }

    void OnDestroy() {
        StopAllCoroutines();
    }

    private IEnumerator PassiveAge() {

        while (true) {
            yield return new WaitForSeconds(passiveAgeInterval);
            AddToAge(passiveAgeIncrement);
        }
    }

    public void AddToAge(float ageToAdd) {

        CurrentAge += ageToAdd;

        movementScalar.Value = 500 * Mathf.Pow(1.1f * CurrentAge + 64, -1.25f) - 0.6f;

        if (CurrentAge >= deathCeilingThreshold || CurrentAge <= 0) {
            StartCoroutine(TriggerDeath());
        }
    }

    IEnumerator TriggerDeath() {

        sound.Play();

        Dying = true;
        ageHitbox.enabled = false;
        movementScalar.Value = 0; // Prevent movement.
        anim.SetTrigger("Death");
        yield return new WaitForSeconds(deathDelay);

        if (transform.root.gameObject.name == "Timepire") {
            SceneManager.LoadScene("Game_Over");
        }

        Destroy(transform.root.gameObject);

    }
}
